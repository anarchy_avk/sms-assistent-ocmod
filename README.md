# sms-assistent.by OpenCart module

This module can be used to send SMS messages from E-commerce platform "OpenCart" via [sms-assistent.by](http://sms-assistent.by) site.

# Installation

* OpenCart 2.3.x  
  * Configure FTP section in OpenCart store  settings  
  * Download last version of module [SMSAssistent.ocmod.zip](https://github.com/Anarchy-avk/sms-assistent-ocmod/releases)
  * Go to "Extension Installer" section of OpenCart admin area
  * Choose "Upload" and select downloaded file, after click "Continue"
  * Next, go to "Modifications", find modification with name "SMS-Assistent Module" and activate them
  * After click refresh button
  * Next step is configure modification 
